# PyFolioStarterModule

A python starter project for building a folio backend module

python3 -m venv pyfoliostarter
source ./pyfoliostarter/bin/activate
pip3 install -r ./requirements.txt 


curl -H X-OKAPI-TENANT:tenanttest2 http://0.0.0.0:8080/v1.0/tenant


python app.py


Notes/refs
https://realpython.com/flask-connexion-rest-api-part-2/

Flask Multi-Tenancy
SO question on MultiTenant with SQLAlchemy:: https://stackoverflow.com/questions/13372001/multi-tenancy-with-sqlalchemy
And here http://gurchet-rai.net/flask-postgres-multitenancy.html

About hot reloading

https://www.reddit.com/r/flask/comments/9iq80p/connexion_how_to_get_flask_to_restart_when/

