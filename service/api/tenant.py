from __future__ import annotations
import connexion

def tenantGet() -> list:
    tenant=None
    if 'X-OKAPI-TENANT' in connexion.request.headers:
      tenant=connexion.request.headers['X-OKAPI-TENANT']
    result = [ { "test":"value", "test2":"value2", "test3":"value3", "tenant":tenant} ]
    return result

def tenantPost() -> list:
    tenant=None
    if 'X-OKAPI-TENANT' in connexion.request.headers:
      tenant=connexion.request.headers['X-OKAPI-TENANT']
    result = [ { "test":"value", "test2":"value2", "test3":"value3", "tenant":tenant, "thisIsPost":"A post method"} ]
    return result

def hello() -> list:
    """
    Don't know what I'm doing
    """
    tenant=None
    if 'X-OKAPI-TENANT' in connexion.request.headers:
      tenant=connexion.request.headers['X-OKAPI-TENANT']
    result = [ { "test":"value", "test2":"value2", "test3":"value3", "tenant":tenant} ]
    return result

def helloPost() -> list:
    """
    Don't know what I'm doing
    """
    tenant=None
    if 'X-OKAPI-TENANT' in connexion.request.headers:
      tenant=connexion.request.headers['X-OKAPI-TENANT']
    result = [ { "test":"value", "test2":"value2", "test3":"value3", "tenant":tenant, "thisIsPost":"A post method"} ]
    return result

def testTwo() -> dict:
    """
    Another test
    """
    result = { "one":"two" }
    return result
