from connexion.resolver import RestyResolver
import connexion

if __name__ == '__main__':
    options = { "swagger_ui": True, "TESTING": True }
    app = connexion.FlaskApp(__name__, specification_dir='swagger/', options=options)
    app.add_api('folio_tenant.yaml', resolver=RestyResolver('api'))

# @app.app because connexion stuffs the flask instance as app - see 
# https://stackoverflow.com/questions/52786596/access-flask-methods-like-before-request-when-using-connexion


@app.app.before_request
def start():
    tenant=None
    if 'X-OKAPI-TENANT' in connexion.request.headers:
      tenant=connexion.request.headers['X-OKAPI-TENANT']
    print('start({})'.format(tenant))
#    g.db = pool.getconn()
#    g.user = session.get('user', None)
#    if 'site' in session:        
#        with g.db.cursor() as cur:
#            cur.execute('SET search_path TO %s', (session['site'],))
#            
            
@app.app.teardown_request
def end(exception):
    print("end()");
#    db = getattr(g, 'db', None)
#    if db is not None:
#        pool.putconn(db)

app.run(port=8080, debug=True)
